CRedir - Crypted Port Redirector.
=================================

This is a port redirector program like the well-known `redir`, but with crypto
added. it uses tweetnacls public key, signing and streamcipher primitives.

it's similar to SSL/TLS, but without a central government^w authority that has
full power over your crypto keys. instead, you generate your own keys using
the `credir-keygen` tool.

in order to use an encrypted tunnel, one side uses credir in client mode, the
other in server mode. each side listens on a dedicated port, which does TCP
forwarding to the other side.

example:
--------

server A with IP 10.1.1.1 provides a tinyproxy HTTP proxy service on its
localhost interface, listening on port 8888. we'll create a listening service
on 0.0.0.0:8889 which decrypts and tunnels incoming connections to tinyproxy.

client B with IP 10.0.0.2 wants to use that proxy, and have a port 8080 open
on its own localhost interface, which proxy-aware applications then can connect
to (for example `curl`). for the proxy-using application, it looks as if
tinyproxy would listen on its own localhost:8080, yet the connection is tunneled
over an encrypted link to server A.

the client generates a keypair with `credir-keygen ~/.credir/MYKEY`,
the server with `credir-keygen -s ~/.credir/MYKEY`,

on client B, execute `cat ~/.credir/MYKEY.pub`.
it prints the public key encoded in hex.
for this example, we assume it is `01020304`.

do the same on server A to get its public key.
for this example, we assume it is `abcdef00`

on server A (inside e.g. a `screen` session), the following command is used:

	credir -k ~/.credir/MYKEY -a 01020304 -i 0.0.0.0 -p 8889 127.0.0.1:8888

on client B:

	credir -c -k ~/.credir/MYKEY -a abcdef00 -i 127.0.0.1 -p 8080 10.1.1.1:8889

implementation
--------------

`credir` is very lightweight, and very light on resources too:

For every client, a thread with a stack size of 8KB is spawned.
the main process basically doesn't consume any resources at all.

The only limits are the amount of file descriptors and the RAM.

It's also designed to be robust: it handles resource exhaustion
gracefully by simply denying new connections, instead of calling abort()
as most other programs do these days.

Another plus is ease-of-use: no config file necessary, everything can be
done from the command line.

cryptographic details:
----------------------

upon connect, the server presents its public signing key, an ephemeral public
ecc key signed with the signing key, a random nonce and a random challenge.
the client encrypts the challenge and a random session key with its secret key
(and the server's ephemeral pubkey, after checking the signature is correct)
and sends it back together with its own pubkey.
the server checks whether the public key is authorized, then decrypts the data
with its own ephemeral secret key and the client's public key.
if the challenge matches, the client has successfully proven its authencity,
and from here on every packet sent back and forth will be encrypted using an
increasing nonce, and the session key using a stream cipher.
this means the handshake consists of only of one package from each side,
which is rather efficient.

command line options
------------------------

    credir -k key -a authkeys [-c -i listenip -p port -t timeout -b bindaddr] ip:port

all arguments except -k and -a are optional.

for client mode, -c needs to be specified.

a single or a list of hex-encoded, colon-separated authorized public keys
need to be supplied using -a.
this assures on the client side that the server isn't mitm'd, an on
the server side that only known clients can use the service.

if -c is given (clientmode), outgoing connections to ip/port will be encrypted.

if -c isn't given (servermode), incoming connections will be decrypted and forwarded.

-k takes the filename prefix of a public key pair generated with credir-keygen
e.g. foo, if a file foo.priv and foo.pub were generated

by default listenip is 0.0.0.0 and port 1080.

option -b specifies the default ip outgoing connections are bound to.

the -t timeout is specified in seconds, default: 0.
if timeout is set to 0, block until the OS cancels the conn. attempt.

all incoming connections will be redirected to ip:port.


build options
-------------

if the macro HAVE_GETRANDOM is specified at build time, linux' getrandom()
syscall will be used instead of opening /dev/urandom. this saves a number
of syscalls everytime entropy is needed, and it makes it possible to use
the program in a chroot without /dev mounted. in case you want to use a chroot
you should also request a static build using LDFLAGS=-static.

example:

    CFLAGS=-DHAVE_GETRANDOM LDFLAGS=-static make

if the option USE_SODIUM is set to 1, libsodium will be used instead of the
embedded tweetnacl library. this will result in a faster binary (producing less
CPU load), but will also result in a bigger binary size if statically compiled.

example:

    make USE_SODIUM=1
